from django.shortcuts import render

def index(request):
  return render(request,'base/index.html')

def indexHome(request):
  return render(request, 'base/home.html')


